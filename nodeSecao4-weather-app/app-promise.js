//READ this::
//Para funcionar esse arquivo foi necessário que fosse configurado o proxy do terminal usando:
//set HTTP_PROXY=http://re037230:Estagio2@192.168.200.246:3128
//set HTTPS_PROXY=http://re037230:Estagio2@192.168.200.246:3128
//http://www.mapquestapi.com/geocoding/v1/address?key=EBGoa8COfzirfEvHfYAqedaedVzionuf&location=1301%20lombard%20street%20philadelphia
//https://api.darksky.net/forecast/47cd820f17e11e20b7b94f403b410492/39.931513,-75.16663
//Put in cmd:
//node app-promise.js -a "1301 lombard street philadelphia"

yargs = require('yargs');
axios = require('axios');
empty = require('./empty');

tunnel = require('tunnel');
 
var tunnelingAgent = tunnel.httpsOverHttp({
    proxy: {
      host: '192.168.200.246',
      port: '3128',
      proxyAuth: 're037230:Estagio2'
    }
  });

const iaxios = axios.create({
   withCredentials: false,
   httpsAgent: tunnelingAgent
});
const argv = yargs
    .options({
        a:{
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for', 
            string: true
        }
    })
    .help()
    .alias('help','h')
    .argv;

var adressEncode = encodeURIComponent(argv.address);
var geocodeUrl = `https://www.mapquestapi.com/geocoding/v1/address?key=EBGoa8COfzirfEvHfYAqedaedVzionuf&location=${adressEncode}`;

//axios funciona somente para https
iaxios.get(geocodeUrl).then((response) => {
    if(response.statusCode === 503){
        throw new Error('Unable to find that address');
    }
  
    console.log(response.data.results[0].providedLocation.location);
    var lat = response.data.results[0].locations[0].latLng.lat;
    var lng = response.data.results[0].locations[0].latLng.lng;
    var weatherUrl = `https://api.darksky.net/forecast/47cd820f17e11e20b7b94f403b410492/${lat},${lng}`;
    var adress = response.data.results[0].providedLocation.location;
    console.log('Adress: ', adress);
    console.log('--------------------------');
    return iaxios.get(weatherUrl);
}).then((response) => {
    var temperature = response.data.currently.temperature;
    var apparentTemperature = response.data.currently.apparentTemperature;
    console.log(`Temperature: ${temperature}. It's feels like ${apparentTemperature}`);
}).catch((e) => {
    if(e.code === 'ENOTFOUND'){
        console.log('Unable to connect to API servers.');
    } else {
        console.log(e.message);   
    }
})