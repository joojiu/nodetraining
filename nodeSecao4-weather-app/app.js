//Para funcionar esse arquivo foi necessário que fosse configurado o proxy do terminal usando:
//set HTTP_PROXY=http://re037230:Estagio2@192.168.200.246:3128
//set HTTPS_PROXY=http://re037230:Estagio2@192.168.200.246:3128

yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');
const argv = yargs
    .options({
        a:{
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for', 
            string: true
        }
    })
    .help()
    .alias('help','h')
    .argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
    if(errorMessage){
        console.log(errorMessage);
    } else {
        console.log(results.address);
        console.log(results.latitude);
        console.log(results.longitude);
        weather.getWeather(results.latitude, results.longitude, (errorMessage, resultsWeather) => {
            if(errorMessage){
                console.log(errorMessage);
            } else {
                console.log(`It's currently ${resultsWeather.temperature}. It feels like ${resultsWeather.apparentTemperature}`);
            }
        })
    }
});

