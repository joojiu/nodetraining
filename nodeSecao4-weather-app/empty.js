//method to check if a variable is empty
emptyCheck = (data) => {
    if(typeof(data) == 'number' || typeof(data) == 'boolean')
    {
      //'Type is number or boolean'
      return false; 
    }
    if(typeof(data) == 'undefined' || data === null)
    {
      //'Type is undefined or data is null'
      return true; 
    }
    if(typeof(data.length) != 'undefined')
    {
      //'Type of the length of the data is not undefined'
      if(/^[\s]*$/.test(data.toString()))
      {
          return true;
      }
      return data.length == 0;
    }
    var count = 0;
    for(var i in data)
    {
      //hasOwnProperty() é um método que perguntar se um objeto tem uma propriedade
      if(data.hasOwnProperty(i))
      {
        count ++;
      }
    }
    //return if the count is == 0 or not (true or false)
    return count == 0;
}

module.exports.emptyCheck = emptyCheck;