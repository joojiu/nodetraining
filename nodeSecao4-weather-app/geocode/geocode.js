
//47cd820f17e11e20b7b94f403b410492
//https://api.darksky.net/forecast/[key]/[latitude],[longitude]

const request = require('request');
const empty = require('../empty');

geocodeAddress = (address, callback) => {
    
    var adressEncode = encodeURIComponent(address);

    request({
        url: `http://www.mapquestapi.com/geocoding/v1/address?key=EBGoa8COfzirfEvHfYAqedaedVzionuf&location=${adressEncode}`,
        json: true
    },(error, response, body) => {
        if(error){
            callback('Unable to connect top Google servers')
        } else if(response.statusCode === 503){
            callback('Unable to find that address');
        } else if(response.statusCode === 200 && !empty.emptyCheck(body.results[0].locations[0].street)){
            callback(undefined, {
                address: body.results[0].providedLocation.location,
                latitude: body.results[0].locations[0].latLng.lat,
                longitude: body.results[0].locations[0].latLng.lng
            })
        }
    });
}

module.exports.geocodeAddress = geocodeAddress;

