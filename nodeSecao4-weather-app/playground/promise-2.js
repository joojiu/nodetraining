const request = require('request');
const empty = require('./../empty');

var geocodeAddress = (address) => {
    return new Promise((resolve, reject) => {
        var adressEncode = encodeURIComponent(address);
        request({
            url: `http://www.mapquestapi.com/geocoding/v1/address?key=EBGoa8COfzirfEvHfYAqedaedVzionuf&location=${adressEncode}`,
            json: true
        },(error, response, body) => {
            console.log(response.statusCode);
            if(error){
                reject('Unable to connect top Google servers');
            } else if(response.statusCode === 503){
                reject('Unable to find that address');
            } else if(response.statusCode === 200 && !empty.emptyCheck(body.results[0].locations[0].street)){
                resolve(
                    `Address: ${body.results[0].providedLocation.location} Longitude: ${body.results[0].locations[0].latLng.lng} Latitude: ${body.results[0].locations[0].latLng.lat}`
                );
            }
        })
    })
};

geocodeAddress("1309 dickinson street philadelphia").then((location) => {
    console.log(JSON.stringify(location, undefined, 2));
}, (errorMessage) => {
    console.log(errorMessage);
});
