//Secao 4 Aula 36
//Intro with Promisses 

//Segundo Exemplo
var addNumbers = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if(typeof a === 'number' && typeof b === 'number'){
                resolve(a + b);
            }
            else{
                reject(`Arguments must to be numbers`);
            }
        }, 1500);
    });
}
console.log(`Arguments must to be numbers`);
addNumbers(7, 5).then((res) => {
    console.log('Result: ', res);
    return addNumbers(res, 33);
}).then((res) => {
    console.log('Should be 45:', res);
}).catch((errorMessage) => {
    console.log(errorMessage);
})

//Primeiro Exemplo
// var somePromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('Hey. It worked!');
//         reject('The message is not validate');
//     }, 2500);
// })

// somePromise.then((message) => {
//     console.log('Sucess: ', message);
// }, (errorMessage) => {
//     console.log('Failed: ', errorMessage);
// })