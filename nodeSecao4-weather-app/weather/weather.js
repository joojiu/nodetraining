const request = require('request');

getWeather = (lat, lng, callback) => {
    request({
        url: `https://api.darksky.net/forecast/47cd820f17e11e20b7b94f403b410492/${lat},${lng}`,
        json: true
    },(error, response, body) => {
        if(error){
            callback('Unable to connect to Forecast.io server.')
        } else if (response.statusCode === 400){
            callback('Unable to fetch weather');
        } else if(!error && response.statusCode === 200){
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            })
        } else{
            callback('Unable to fetch weather');
        }
    });
}

module.exports.getWeather = getWeather;