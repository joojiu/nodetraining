
$ ssh-keygen -t rsa -b 4096 -C 'uedajooji@gmail.com'

$ ls -al ~/.ssh

$ eval "$(ssh-agent -s)"

$ssh-add ~/.ssh/id_rsa
=>Identity added: /c/Users/re036598/.ssh/id_rsa (/c/Users/re036598/.ssh/id_rsa)

$ clip < ~/.ssh/id_rsa.pub
//copy in the ssh keys config in https://bitbucket.org/account/user/joojiu/ssh-keys/

$ ssh -T git@bitbucket.com
=>The authenticity of host 'github.com (192.30.253.112)' can't be established.
=>RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.
=>Are you sure you want to continue connecting (yes/no)? yes
=>Warning: Permanently added 'github.com,192.30.253.112' (RSA) to the list of known hosts.
=>git@github.com: Permission denied (publickey).

OR::
See the https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html for check if it's gonna be write

OBS::
$ git push
//se pedir para autenticar é porque não deu certo, o problema pode estar no origin que foi setado

//para verificar qual o nome do repositório
$ git remote -v
=>origin  https://joojiu@bitbucket.org/joojiu/nodetraining.git (fetch)
=>origin  https://joojiu@bitbucket.org/joojiu/nodetraining.git (push)
PARA (forma correta)::
=>origin  git@bitbucket.org:joojiu/nodetraining.git (fetch)
=>origin  git@bitbucket.org:joojiu/nodetraining.git (push)

$ git remote remove origin

$ git remote add origin git@bitbucket.org:joojiu/nodetraining.git

//Depois disso ele irá funcionar perfeitamente